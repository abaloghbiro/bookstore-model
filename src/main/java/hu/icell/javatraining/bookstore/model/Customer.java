package hu.icell.javatraining.bookstore.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
public class Customer extends BusinessObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String firstName;

	private String lastName;

	@OneToMany
	private List<Book> ownage = new ArrayList<>();

	public void addBook(Book book) {
		ownage.add(book);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<Book> getOwnage() {
		return ownage;
	}

	public void setOwnage(List<Book> ownage) {
		this.ownage = ownage;
	}

}
